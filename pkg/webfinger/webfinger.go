package webfinger

import (
	"encoding/json"
	"io"
)

// Resource provides the fields for a JRD as specified in RFC7033.
type Resource struct {
	Subject    string            `json:"subject"`
	Aliases    []string          `json:"aliases,omitempty"`
	Properties map[string]string `json:"properties,omitempty"`
	Links      []Link            `json:"links,omitempty"`
}

// Link provides the fields for a link entry in a JRD.
type Link struct {
	Rel  string `json:"rel"`
	Href string `json:"href"`
}

// Decoder provides a context for decoding a JRD.
type Decoder struct {
	reader io.Reader
}

// NewDecoder creates a Decoder given an I/O reader.
func NewDecoder(r io.Reader) (*Decoder, error) {
	dec := &Decoder{reader: r}
	return dec, nil
}

// Decode decodes a JRD into a Resource.
func (dec *Decoder) Decode(rs *Resource) error {
	jdec := json.NewDecoder(dec.reader)
	return jdec.Decode(rs)
}

// Encoder provides a context for encoding a JRD.
type Encoder struct {
	writer io.Writer
}

// NewEncoder creates an Encoder given an I/O writer.
func NewEncoder(w io.Writer) (*Encoder, error) {
	enc := &Encoder{writer: w}
	return enc, nil
}

// Encode encodes a Resource into a JRD.
func (enc *Encoder) Encode(rs *Resource) error {
	jenc := json.NewEncoder(enc.writer)
	return jenc.Encode(*rs)
}
