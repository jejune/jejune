package webfinger

import (
	"bytes"
	"testing"
	"github.com/stretchr/testify/require"
)

func TestRoundTrip(t *testing.T) {
	jrd := Resource{
		Subject: "acct:ariadne@jejune.dev",
		Links: []Link{
			{Rel: "application/activity+json", Href: "https://jejune.dev/.jejune/ariadne"},
		},
	}

	encbuf := &bytes.Buffer{}
	enc, err := NewEncoder(encbuf)
	require.NoErrorf(t, err, "got error while constructing the encoder: %s", err)

	err = enc.Encode(&jrd)
	require.NoErrorf(t, err, "got error while encoding: %s", err)

	dec, err := NewDecoder(encbuf)
	require.NoErrorf(t, err, "got error while constructing the decoder: %s", err)

	var decjrd Resource
	err = dec.Decode(&decjrd)
	require.NoErrorf(t, err, "got error while decoding: %s", err)

	require.Equal(t, jrd.Subject, decjrd.Subject)
	require.Equal(t, 1, len(decjrd.Links))
	require.Equal(t, jrd.Links[0], decjrd.Links[0])
}
